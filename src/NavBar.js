import React from 'react';
import logo from './logo.svg';
import {
  Navbar,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  Jumbotron,
  Container, Card, Button, CardImg, CardTitle, CardText, CardDeck,
 CardSubtitle, CardBody
  } from 'reactstrap';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import './App.css';
import 'bootstrap/dist/css/bootstrap.css';
import Locations from './pages/locations/Locations'
import Health from './pages/health/Health'
import AirQuality from './pages/airquality/AirQuality'
import About from './pages/About'
import Home from './App';
import {Health0,Health1,Health2} from './pages/health/Health';
import {Locations0,Locations1,Locations2} from './pages/locations/Locations';
import {AirQuality0,AirQuality1,AirQuality2} from './pages/airquality/AirQuality';


export default class NavBar extends React.Component {
  render(){
    return (
      <div>
        <Router>
          <Navbar color="dark" dark>
            <NavbarBrand href="/">How's my air? </NavbarBrand>
              <Nav pills>
                <NavItem>
                  <NavLink href="/Locations">Locations</NavLink>
                </NavItem>
                <NavItem>
                  <NavLink href="/Health">Health</NavLink>
                </NavItem>
                <NavItem>
                  <NavLink href="/AirQuality">Air Quality</NavLink>
                </NavItem>
                <NavItem>
                  <NavLink href="/About">About</NavLink>
                </NavItem>
              </Nav>
          </Navbar>
        <Route exact path="/" component={Home} />
        <Route path="/Locations" component={Locations} />
        <Route path="/AirQuality" component={AirQuality} />
        <Route path="/Health" component={Health} />
        <Route path="/Health0" component={Health0} />        
        <Route path="/Health1" component={Health1} />        
        <Route path="/Health2" component={Health2} />        
        <Route path="/Locations0" component={Locations0} />        
        <Route path="/Locations1" component={Locations1} />        
        <Route path="/Locations2" component={Locations2} />                
        <Route path="/AirQuality0" component={AirQuality0} />        
        <Route path="/AirQuality1" component={AirQuality1} />        
        <Route path="/AirQuality2" component={AirQuality2} />                
        <Route path="/About" component={About} />
        </Router>
      </div>
    );
  }
}
