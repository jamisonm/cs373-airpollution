import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import { Jumbotron, Container, Card, Button, CardImg, CardTitle, CardText,
   CardDeck, CardSubtitle, CardBody , NavLink ,UncontrolledCarousel, Form,FormGroup, Label, Input, FormText} from 'reactstrap';
import { Image, Col, Row } from 'react-bootstrap'; 
import { BrowserRouter as Router, Route, Link ,  Switch,BrowserHistory} from 'react-router-dom';
import { createBrowserHistory } from 'history';
import ReactDOM from 'react-dom';
import Health from '../health/Health';
import {Health0,Health1,Health2} from '../health/Health';
import Locations from '../locations/Locations';
import {Locations0,Locations1,Locations2} from '../locations/Locations';
import Home from '../../App';
import About from '../About';

export class AirQuality0 extends Component {
    render() {
       return (
         <div>
           <CardDeck>
            <Card color="light">
              <CardBody>
              <CardTitle>OZONE AQI</CardTitle>
                <CardText>
                <div>Description: Ground level Ozone (O3)</div>                                                                                  
                <div>Range: 0-500</div>
                <div>Severity Levels: good, moderate, unhealthy for sensitive groups, unhealthy, very unhealthy, hazardous</div>
                <div>Sources: Pollutants reacting in the presence of sunlight</div>                            
                <div><NavLink href="/Locations0">Locations at risk</NavLink></div>
                <div><NavLink href="/Health0">Associated Health Risks</NavLink></div>                
              </CardText>
              <CardImg top width="25%" src="https://www.verywellhealth.com/thmb/fqyEsd_p7S5S_v5m5pDoPwaDw9I=/3102x3102/smart/filters:no_upscale()/lung-showing-bronchitis--blockages-made-visible-by-bronchography-128117767-594441723df78c537beb3c96.jpg" alt="Card image cap" />
            </CardBody>  
          </Card>
         </CardDeck>
         </div>
       );
   }
}

export class AirQuality1 extends Component {
    render() {
       return (
         <div>
           <CardDeck>
            <Card color="light">
              <CardBody>
              <CardTitle>PM2.5</CardTitle>
                <CardText>
                <div>Description: Inhalible Fine Particles (2.5 Microns)</div>                                                                                  
                <div>Range: 0-500</div>
                <div>Severity Levels: good, moderate, unhealthy for sensitive groups, unhealthy, very unhealthy, hazardous</div>
                <div>Sources: Burning of Fossil Fuels and Organic Matter: </div>                              
                <div><NavLink href="/Locations0">Locations at risk</NavLink></div>
                <div><NavLink href="/Health0">Associated Health Risks</NavLink></div>                
              </CardText>
              <CardImg top width="25%" src="https://www.verywellhealth.com/thmb/fqyEsd_p7S5S_v5m5pDoPwaDw9I=/3102x3102/smart/filters:no_upscale()/lung-showing-bronchitis--blockages-made-visible-by-bronchography-128117767-594441723df78c537beb3c96.jpg" alt="Card image cap" />
            </CardBody>  
          </Card>
         </CardDeck>
         </div>

       );
   }
}

export class AirQuality2 extends Component {
    render() {
       return (
         <div>
           <CardDeck>
            <Card color="light">
              <CardBody>
              <CardTitle>PM10</CardTitle>
                <CardText>
                <div>Description: Inhalible Large Coarse Particles (10 Microns)</div>                                                                                  
                <div>Range:0-500</div>
                <div>Severity Levels: good, moderate, unhealthy for sensitive groups, unhealthy, very unhealthy, hazardous</div>
                <div>Sources: Sea Salt and Combustion and Dust</div>                              
                <div><NavLink href="/Locations0">Locations at risk</NavLink></div>
                <div><NavLink href="/Health0">Associated Health Risks</NavLink></div>                
              </CardText>
              <CardImg top width="25%" src="https://www.verywellhealth.com/thmb/fqyEsd_p7S5S_v5m5pDoPwaDw9I=/3102x3102/smart/filters:no_upscale()/lung-showing-bronchitis--blockages-made-visible-by-bronchography-128117767-594441723df78c537beb3c96.jpg" alt="Card image cap" />
            </CardBody>  
          </Card>
         </CardDeck>
         </div>

       );
   }
}

export class AirQualityData extends Component {
   render() {
      return (
         <div>
            <Router>
               <CardDeck>
                  <Card>
                     <CardBody>
                        <NavLink href="/AirQuality0">Ozone</NavLink>
                     </CardBody>
                  </Card>
                  <Card>
                     <CardBody>
                        <NavLink href="/AirQuality1">PM2.5</NavLink>
                     </CardBody>
                  </Card>
                  <Card>
                     <CardBody>
                        <NavLink href="/AirQuality2">PM10</NavLink>
                     </CardBody>
                  </Card>
               </CardDeck>
               <Route path="/AirQuality0" component={AirQuality0} />
               <Route path="/AirQuality1" component={AirQuality1} />
               <Route path="/AirQuality2" component={AirQuality2} />
            </Router>
         </div>
      );
   }
}

export default class AirQuality extends Component {
    render() {
       return (
         <div>
          <AirQualityData/>
           <Jumbotron fluid>
             <Container>
                <h1 className="display-3">AirQuality</h1>
             <img src="https://timedotcom.files.wordpress.com/2016/01/beijing-air-pollution.jpeg" class="img-fluid" alt="Responsive image"></img>
             </Container>
           </Jumbotron>
         </div>
       );
   }
}
