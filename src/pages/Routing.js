
import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import { Jumbotron, Container, Card, Button, CardImg, CardTitle, CardText,
   CardDeck, CardSubtitle, CardBody , NavLink ,UncontrolledCarousel, Form, FormGroup, Label, Input, FormText} from 'reactstrap';
import { Image, Col, Row } from 'react-bootstrap'; 
import { BrowserRouter as Router, Route, Link , BrowserHistory} from 'react-router-dom';
import ReactDOM from 'react-dom';
import {Health0,Health1,Health2} from './health/Health';
import {NewYork,Shanghai,Chicago} from './locations/Locations';
import Health from './health/Health';
import Locations from './locations/Locations';
import { createBrowserHistory } from 'history';

export default class Routing extends Component {
  render() {
    return (
    	<Router>
	      	<Route path="/Health" component={Health} />           
	       	<Route path="/Health0" component={Health0} />           
	       	<Route path="/Health1" component={Health1} />           
	       	<Route path="/Health2" component={Health2} />           
	       	<Route path="/Locations" component={Locations} />           
	       	<Route path="/Shanghai" component={Shanghai} />
	       	<Route path="/Chicago" component={Chicago} />
	       	<Route path="/New York" component={NewYork} />
       	</Router>
    );
  }
}

