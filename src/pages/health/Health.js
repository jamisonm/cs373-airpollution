import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import { Jumbotron, Container, Card, Button, CardImg, CardTitle, CardText,
   CardDeck, CardSubtitle, CardBody , NavLink ,UncontrolledCarousel, Form,FormGroup, Label, Input, FormText} from 'reactstrap';
import { Image, Col, Row } from 'react-bootstrap'; 
import { BrowserRouter as Router, Route, Link ,  Switch,BrowserHistory} from 'react-router-dom';
import { createBrowserHistory } from 'history';
import ReactDOM from 'react-dom';
import Locations from '../locations/Locations';
import {Locations0,Locations1,Locations2} from '../locations/Locations';
import AirQuality from '../airquality/AirQuality';
import {AirQuality0,AirQuality1,AirQuality2} from '../airquality/AirQuality';
import Home from '../../App';
import About from '../About';


export class Health0 extends Component {
   render() {
      return (
         <div>
           <CardDeck>
            <Card color="light">
              <CardBody>
              <CardTitle>Bronchitis</CardTitle>
                <CardText>
                <div>Description: Bronchitis occurs when the airways of the lungs swell and produce mucus.</div>                                                                                  
                <div>Symptomss: Soreness in the chest, Fatigue (feeling tired), Mild headache, Mild body aches, Watery eyes, Sore throat</div> 
                <div>Causes: Acute bronchitis is usually caused by a virus and often occurs after an upper respiratory infection. Bacteria can sometimes cause acute bronchitis, but even in these cases antibiotics are NOT recommended and will not help you get better.</div>
                <div>Treatment: Get plenty of rest, Drink plenty of fluids, Use a clean humidifier or cool mist vaporizer, Breathe in steam from a bowl of hot water or shower,over-the-counter medicines </div>              
                <div><NavLink href="/Locations0">Locations to avoid</NavLink></div>
                <div><NavLink href="/AirQuality0">Pollutants</NavLink></div>                
              </CardText>
              <CardImg top width="25%" src="https://www.verywellhealth.com/thmb/fqyEsd_p7S5S_v5m5pDoPwaDw9I=/3102x3102/smart/filters:no_upscale()/lung-showing-bronchitis--blockages-made-visible-by-bronchography-128117767-594441723df78c537beb3c96.jpg" alt="Card image cap" />
            </CardBody>  
          </Card>
         </CardDeck>
         </div>
      );
   }
}

export class Health1 extends Component {
   render() {
      return (
         <div>
           <CardDeck>
            <Card color="light">
              <CardBody>
              <CardTitle>Asthma</CardTitle>
                <CardText>
                <div>Description: Asthma is a disease that affects your lungs.</div>                                                                                  
                <div>Symptoms: An asthma attack may include coughing, chest tightness, wheezing, and trouble breathing.</div> 
                <div>Causes: Some of the most common triggers are tobacco smoke, dust mites, outdoor air pollution, cockroach allergen, pets, mold, smoke from burning wood or grass, and infections like flu.</div>
                <div>Treatment: Take your medicine exactly as your doctor tells you and stay away from things that can trigger an attack to control your asthma.</div>              
                <div><NavLink href="/Locations1">Locations to avoid</NavLink></div>
                <div><NavLink href="/AirQuality1">Pollutants</NavLink></div>                
              </CardText>
              <CardImg top width="25%" src="https://foodallergycanada.ca/wp-content/uploads/asthma-3.jpg" alt="Card image cap" />
            </CardBody>  
          </Card>
         </CardDeck>
         </div>
      );
   }
}

export class Health2 extends Component {
   render() {
      return (
         <div>
           <CardDeck>
            <Card color="light">
              <CardBody>
              <CardTitle>Tuberculosis</CardTitle>
                <CardText>
                <div>Description: Tuberculosis (TB) is caused by a bacterium called Mycobacterium tuberculosis. The bacteria usually attack the lungs, but TB bacteria can attack any part of the body such as the kidney, spine, and brain.</div>                                                                                  
                <div>Symptoms: a bad cough that lasts 3 weeks or longer, pain in the chest,coughing up blood or sputum (phlegm from deep inside the lungs)</div> 
                <div>Causes: TB bacteria are spread through the air from one person to another. The TB bacteria are put into the air when a person with TB disease of the lungs or throat coughs, speaks, or sings. People nearby may breathe in these bacteria and become infected.</div>                
                <div>Treatment: first-line anti-TB agents that form the core of treatment regimens are: isoniazid (INH), rifampin (RIF), ethambutol (EMB), pyrazinamide (PZA)</div>              
                <div><NavLink href="/Locations2">Locations to avoid</NavLink></div>
                <div><NavLink href="/AirQuality2">Pollutants</NavLink></div>                
              </CardText>
              <CardImg top width="25%" src="https://prod-images.static.radiopaedia.org/images/1429785/d815245ec98b5f0efc73b89daef72c_jumbo.jpg" alt="Card image cap" />
            </CardBody>  
          </Card>
         </CardDeck>
         </div>
      );
   }
}

export class Sources extends Component {
   render() {
      return (
         <div>
         Sources:
         <ul>
            <li> http://www.sparetheair.com/health.cfm </li>
         </ul>
         </div>
      );
   }
}

export class HealthData extends Component {
   render() {
      return (
         <div>
               <CardDeck>
                  <Card>
                     <CardBody>
                        <NavLink href="/Health0">Bronchitis</NavLink>
                     </CardBody>
                  </Card>
                  <Card>
                     <CardBody>
                        <NavLink href="/Health1">Asthma</NavLink>
                     </CardBody>
                  </Card>
                  <Card>
                     <CardBody>
                        <NavLink href="/Health2">Tuberculosis</NavLink>
                     </CardBody>
                  </Card>
               </CardDeck>
         </div>
      );
   }
}


export default class Health extends Component {
   render() {
      return (
         <div>
         <HealthData/>
         <Jumbotron fluid>
         <Container>
         <h1 className="display-3">Health</h1>
         <img src="https://hhp-blog.s3.amazonaws.com/2018/02/iStock-639896942.jpg" class="img-fluid" alt="Responsive image"></img>
         </Container>
         </Jumbotron>
         </div>
      );
   }
}
