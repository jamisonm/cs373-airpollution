import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import { Jumbotron, Container, Card, Button, CardImg, CardTitle, CardText,
   CardDeck, CardSubtitle, CardBody , NavLink ,UncontrolledCarousel, Form,FormGroup, Label, Input, FormText} from 'reactstrap';
import { Image, Col, Row } from 'react-bootstrap'; 
import { BrowserRouter as Router, Route, Link ,  Switch,BrowserHistory} from 'react-router-dom';
import { createBrowserHistory } from 'history';
import ReactDOM from 'react-dom';
import Health from '../health/Health';
import {Health0,Health1,Health2} from '../health/Health';
import AirQuality from '../airquality/AirQuality';
import {AirQuality0,AirQuality1,AirQuality2} from '../airquality/AirQuality';
import Home from '../../App';
import About from '../About';

export class Locations0 extends Component {
   render() {
      return (
           <CardDeck>
            <Card color="light">
              <CardBody>
              <CardTitle>Shanghai</CardTitle>
                <CardText>
                <div>Population: 26,317,104</div>
                <div>Description: The largest city in China by population, and the largest city proper in the world, with a population of 26.3 million as of 2019.</div>                                                                                  
                <div>Lattidude: 31.230391</div> 
                <div>Longitude: 121.473701</div>                
                <div><NavLink href="/Health0">Possible Health Issues</NavLink></div>
                <div><NavLink href="/AirQuality0">Pollutants</NavLink></div>                
              </CardText>
              <CardImg top width="25%" src="https://www.tripsavvy.com/thmb/nT0SZLOEKVgePvsGdHDIyqDOcY4=/950x0/filters:no_upscale():max_bytes(150000):strip_icc()/China-Shanghai-530230383-56a0329a5f9b58eba4af4eb9.jpg" alt="Card image cap" />
            </CardBody>  
          </Card>
         </CardDeck>
      );
   }
}



export class Locations1 extends Component {
   render() {
      return (
            <Card color="light">
            <CardTitle>Chicago</CardTitle>            
              <CardBody>
                <CardText>               
                <div>Population: 2,695,598</div>
                <div>Description: Chicago is the most populous city in Illinois, as well as the third most populous city in the United States.</div>                                                                                  
                <div>Lattidude: 41.878113</div> 
                <div>Longitude: -87.629799</div>
                <div><NavLink href="/Health1">Possible Health Issues</NavLink></div>
                <div><NavLink href="/AirQuality1">Pollutants</NavLink></div>                
              </CardText>
              <CardImg top width="25%" src="https://thekittchen.com/wp-content/uploads/2017/10/Ultimate-Chicago-Travel-Guide-4.jpg" alt="Card image cap"/>
            </CardBody>  
          </Card>
      );
   }
}

export class Locations2 extends Component {
   render() {
      return (
            <Card color="light">
              <CardBody>
              <CardTitle>New York</CardTitle>            
                <CardText>
                <div>Population: 8,175,133</div>
                <div>Description: The most populous city in the United States.</div>                                                                                  
                <div>Lattidude: 40.712776</div> 
                <div>Longitude: -74.005974</div>
                <div><NavLink href="/Health2">Possible Health Issues</NavLink></div>
                <div><NavLink href="/AirQuality2">Pollutants</NavLink></div>                
              </CardText>
              <CardImg top width="25%" src="https://si.wsj.net/public/resources/images/B3-CJ148_AIRPOR_8SR_20181112225920.jpg" alt="Card image cap" />
            </CardBody>  
          </Card>
      );
   }
}

export class Sources extends Component {
   render() {
      return (
         <div>
         Sources:
         <ul>
            <li> http://www.sparetheair.com/Locations.cfm </li>
         </ul>
         </div>
      );
   }
}

export default class Locations extends Component {
    render() {
      // createBrowserHistory.push('/');
      return (
        <div>
           <CardDeck>
              <Card>
                 <CardBody>
                    <NavLink href="/Locations0">Shanghai</NavLink>
                 </CardBody>
              </Card>
              <Card>
                 <CardBody>
                    <NavLink href="/Locations1">Chicago</NavLink>
                 </CardBody>
              </Card>
              <Card>
                 <CardBody>
                    <NavLink href="/Locations2">New York</NavLink>
                 </CardBody>
              </Card>
           </CardDeck>
        <Jumbotron fluid>
         <Container>
            <h1 className="display-3">Locations</h1>
         <img src="https://www.globe.gov/globe-gov-measurements-portlet/img/map-background.png" class="img-fluid" alt="Responsive image"></img>
         </Container>
        </Jumbotron>
       </div>
      );
  }
}


